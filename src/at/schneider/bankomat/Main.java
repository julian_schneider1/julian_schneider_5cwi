package at.schneider.bankomat;

import java.util.Scanner;
// https://docs.oracle.com/javase/7/docs/api/java/util/Scanner.html //

// HILFESTELLUNG //
// https://www.sanfoundry.com/java-program-display-atm-transaction //

public class Main
{
    public static void main(String args[] )
    { 
        int balance = 5000, withdraw, deposit;
        Scanner s = new Scanner(System.in);
        while(true)
        {
            System.out.println("Bankomat");
            System.out.println("W�hlen Sie 1 um Geld auszuzahlen");
            System.out.println("W�hlen Sie 2 um Geld einzuzahlen");
            System.out.println("W�hlen Sie 3 um Ihren Kontostand abzurufen");
            System.out.println("W�hlen Sie 4 um zu verlassen");
            System.out.print("Bitte w�hlen sie eine Aktion");
            int n = s.nextInt();
            switch(n)
            {
                case 1:
                System.out.print("Geben Sie Ihren gew�nschten Auszahlungsbetrag an:");
                withdraw = s.nextInt();
                if(balance >= withdraw)
                {
                    balance = balance - withdraw;
                    System.out.println("Bitte entnehmen Sie ihr Geld");
                }
                else
                {
                    System.out.println("Ung�ltiger Kontostand");
                }
                System.out.println("");
                break;
 
                case 2:
                System.out.print("Betrag eingeben:");
                deposit = s.nextInt();
                balance = balance + deposit;
                System.out.println("Ihr Geld wurde erfolgreich einbezahlt");
                System.out.println("");
                break;
 
                case 3:
                System.out.println("Balance : "+balance);
                System.out.println("");
                break;
 
                case 4:
                System.exit(0);
            }
        }
    }
}
