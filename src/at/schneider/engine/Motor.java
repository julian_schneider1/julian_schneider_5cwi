package at.schneider.engine;

public interface Motor {
	public void run(int amount);
	public int getSerial();
}
