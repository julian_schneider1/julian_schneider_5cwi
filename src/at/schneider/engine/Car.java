package at.schneider.engine;

public class Car {
	
	// Car has a var=useEngine with type Motor=TopDiesel/SuperDiesel 
	private Motor useEngine;
	
	// Constructor for Car (
	public Car(Motor useEngine) {
		super();
		this.useEngine = useEngine;
	}
	
	// Car has function which calculates the amount
	public void gas(int amount) {
		int newAmount = amount/2;
		this.useEngine.run(newAmount);
	}
	
	// Car has function who gets the serial number
	public void getSerial() {
		System.out.println("Dieser Motor hat die Seriennummer: " + this.useEngine.getSerial());
	}
}
