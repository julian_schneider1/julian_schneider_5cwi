package at.schneider.tictactoe;

import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
	
		// boolean um herauszufinden welcher Spieler am Zug ist //
		boolean player1Turn = true;
		
		// Erstellen eines neuen TicTacToe Spiels
		TicTacToe t1 = new TicTacToe();
		
		// Spieleabfolge //
		while(true) {
			t1.showField();
			String[] selectedField = t1.input();
			t1.fillInBoard(selectedField, player1Turn);
			t1.checkWin();
			player1Turn = !player1Turn;
		}
	}
}
