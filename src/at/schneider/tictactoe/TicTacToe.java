package at.schneider.tictactoe;

import java.util.Scanner;

public class TicTacToe {

	// Definition des Spielfeldes //
	int[][] field = new int[3][3];
	
	// neuer Scanner um Werte einzugeben // 
	Scanner s = new Scanner(System.in);

	
	// function um Spielfeld anzuzeigen //
	public void showField() {

		System.out.println(field[0][0] + "|" + field[0][1] + "|" + field[0][2]);
		System.out.println(field[1][0] + "|" + field[1][1] + "|" + field[1][2]);
		System.out.println(field[2][0] + "|" + field[2][1] + "|" + field[2][2]);

	}

	// input "verwendbar" machen // 
	public String[] input() {

		String input = s.next();
		String[] inputString = input.split(",");

		return inputString;

	}

	// einsetzen von Werten ins Spielfeld //
	public void fillInBoard(String[] selectedField, boolean player1Turn) {

		int column = 0;
		int row = 0;
		try {
			column = Integer.parseInt(selectedField[0]);
			row = Integer.parseInt(selectedField[1]);
		} catch (NumberFormatException e) {

			System.out.println("Falsche Eingabe. Richtig: z.B: 0,1");

		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("Falsche Eingabe. Richtig: z.B: 0,1");
		}

		if (player1Turn == true) {field[row][column] = 1;} 
		else {field[row][column] = 2;}
	}

	// function um herauszufinden ob ein Spieler gewonnen hat //
	public void checkWin() {
		
		boolean didWin = false;
		
		if(     field[0][0] == field[1][0] && field[1][0] == field[2][0] && field[0][0] > 0) {didWin = true;}
		else if(field[0][1] == field[1][1] && field[1][1] == field[2][1] && field[0][1] > 0) {didWin = true;}
		else if(field[0][2] == field[1][2] && field[1][2] == field[2][2] && field[0][2] > 0) {didWin = true;}
		
		else if(field[0][0] == field[0][1] && field[0][1] == field[0][2] && field[0][0] > 0) {didWin = true;}
		else if(field[1][0] == field[1][1] && field[1][1] == field[1][2] && field[1][0] > 0) {didWin = true;}
		else if(field[2][0] == field[2][1] && field[2][1] == field[2][2] && field[2][0] > 0) {didWin = true;}

		else if(field[0][0] == field[1][1] && field[1][1] == field[2][2] && field[0][0] > 0) {didWin = true;}
		else if(field[2][0] == field[1][1] && field[1][1] == field[0][2] && field[2][0] > 0) {didWin = true;}
		
		if (didWin == true) {
			System.out.println("Gewonnen");
		}
	}
}

