package at.schneider.factorypattern;

public class main {

	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Gamer g = new Gamer();
		Homer h1 = new Homer();
		RandomActor ra = new RandomActor();
		
		h1.move();
		ra.createActor();
		g.addActor(ra.createActor());
		g.renderAll();
	}

}