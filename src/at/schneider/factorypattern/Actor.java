package at.schneider.factorypattern;

public interface Actor {
	public void move();
	
	public void render();
}