package at.schneider.factorypattern;

import java.util.ArrayList;
import java.util.List;

public class Gamer {
	private List<Actor> actorList;

	public Gamer() {
		super();
		this.actorList = new ArrayList<>();
	}
	public void addActor(Actor actorName) {
		actorList.add(actorName);
	}
	public void renderAll() {
		for (Actor actor : actorList) {
			actor.render();
		}
	}
	public void moveAll() {
		for (Actor actor : actorList) {
			actor.move();
		}
	}
	
}