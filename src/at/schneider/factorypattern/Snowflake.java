package at.schneider.factorypattern;

public class Snowflake implements Actor {

	@Override
	public void move() {
		// TODO Auto-generated method stub
		System.out.println("Snowflake is falling down.");
	}

	@Override
	public void render() {
		// TODO Auto-generated method stub
		System.out.println("Snowflake rendered.");
	}

}