package at.schneider.factorypattern;

public class Homer implements Actor{

	@Override
	public void move() {
		// TODO Auto-generated method stub
		System.out.println("Homer is moving around.");
	}

	@Override
	public void render() {
		// TODO Auto-generated method stub
		System.out.println("Homer rendered.");
	}

}