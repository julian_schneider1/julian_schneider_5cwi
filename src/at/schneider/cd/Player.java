package at.schneider.cd;

import java.util.ArrayList;
import java.util.List;

public class Player implements Playable {
	private List<Playable> music;
	
	
	public Player() {
		super();
		// TODO Auto-generated constructor stub
		this.music = new ArrayList<Playable>();
	}

	public void addMusic(Playable music) {
		this.music.add(music);
	}
	
	@Override
	public void play() {
		// TODO Auto-generated method stub
		
	}
	
	public void playAll() {
		// TODO Auto-generated method stub
		for(Playable playable : music) {
			playable.play();
		}
	}

}