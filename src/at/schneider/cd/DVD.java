package at.schneider.cd;

import java.util.ArrayList;
import java.util.List;

public class DVD {
	private String name;
	private List<Title> titles;

	public DVD(String name) {
		super();
		this.name = name;
		this.titles = new ArrayList<Title>();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Title> getTitles() {
		return titles;
	}

	public void setTitles(List<Title> titles) {
		this.titles = titles;
	}
	
	public void addTitle(Title titles) {
		this.titles.add(titles);
	}
	
}