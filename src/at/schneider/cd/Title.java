package at.schneider.cd;

public class Title extends DVD implements Playable {
	
	public Title(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void play() {
		// TODO Auto-generated method stub
		System.out.println("Playing the title " + getName() + ".");
	}

}