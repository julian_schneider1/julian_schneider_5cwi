package at.schneider.cd;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Player p = new Player();
		
		CD cd1 = new CD("myCD");
		DVD dvd1 = new DVD("myDVD");
		
		Song s1 = new Song("'DanzaKoduro'");
		Title t1 = new Title("'DanzaKoduro1'");
	
		cd1.addSong(s1);
		dvd1.addTitle(t1);
		
		p.addMusic(t1);
		p.addMusic(s1);
		
		p.playAll();
	}
	
}