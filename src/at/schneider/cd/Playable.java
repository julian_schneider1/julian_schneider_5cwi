package at.schneider.cd;

public interface Playable {
	public void play();
}