package at.schneider.observer;

import java.util.ArrayList;
import java.util.List;

public class Sensor{
	
	private List<Observerbale> observables;

	public Sensor() {
		super();
		this.observables = new ArrayList<Observerbale>();
	}
	public void addObserverbale(Observerbale os) {
		observables.add(os);
	}
	
	public void informAll() {
		for (Observerbale  : observables) {
			observable.inform();
		}
	}
}